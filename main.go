package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

// ProcessLine : searches for old in lin to replace it by new
// It returns found=true if the pattern was found, res with the resulting string
//and occ with the number of occurence of old
func ProcessLine(line, old, new string) (found bool, res string, occ int) {
	oldLower := strings.ToLower(old)
	newLower := strings.ToLower(new)
	res = line

	if strings.Contains(line, old) || strings.Contains(line, oldLower) {
		found = true
		occ += strings.Count(line, old)
		res = strings.Replace(line, old, new, -1)
		res = strings.Replace(res, oldLower, newLower, -1)
	}

	return found, res, occ
}

func FindReplaceFile(src, dest, old, new string) (occ int,
	lines []int, err error) {
	srcFile, err := os.Open(src)
	defer srcFile.Close()
	if err != nil {
		return occ, lines, err
	}
	dstFile, err := os.Create(dest)
	defer dstFile.Close()

	if err != nil {
		return occ, lines, err
	}
	old = old + " "
	new = new + " "
	lineIdx := 1
	scanner := bufio.NewScanner(srcFile)
	writter := bufio.NewWriter(dstFile)
	defer writter.Flush()
	for scanner.Scan() {
		found, res, o := ProcessLine(scanner.Text(), old, new)
		if found {
			occ += o
			lines = append(lines, lineIdx)
		}
		//fmt.Println(res)
		fmt.Fprintf(writter, res)
		lineIdx++
	}
	return occ, lines, nil
}

func main() {
	/*found, res, occ := ProcessLine("test lalalalalala gogogogog zfgunboebf sifhb go", "go", "python")
	fmt.Println(found, res, occ)*/

	old := "Go"
	new := "Python"

	occ, lines, err := FindReplaceFile("wiki.txt", "wikipython.txt", old, new)
	if err != nil {
		fmt.Printf("Error while executing find replace: %v\n", err)
		return
	}

	fmt.Println("Summary")
	defer fmt.Println("End of summary")

	fmt.Printf("Number of occurences of %v: %v\n", old, occ)
	fmt.Printf("Number of lines : %d\n", len(lines))
	fmt.Print("Lines : [")
	for i, l := range lines {
		fmt.Printf("%v", l)
		if i < len(lines)-1 {
			fmt.Print(" - ")
		}
	}
	fmt.Println("]")

}
